#!/bin/bash

if ! which g++ > /dev/null 2>/dev/null; then
  echo "Ensure g++ is installed and supports C++23."
  exit 1
fi
if ! which make > /dev/null 2>/dev/null; then
  echo "Ensure make is installed."
  exit 1
fi
if ! which nproc > /dev/null 2>/dev/null; then
  echo "Ensure the coreutils are installed."
  exit 1
fi

if ! make -j`nproc` bin/sentinel-evolution; then
  echo 1>&2
  echo "Build failed for some unknown reason." 1>&2
  echo "Ensure you have the libsdl2-dev package installed." 1>&2
  exit 1
fi

echo "This demo shows the evolution of a bot controlled by a neural net, learning how to walk on four legs."
echo "Initially it will not get far, but after some thousands of generations, it will quickly hop forwards indefinitely."
echo "The screen wil pan upwards to keep up with the little bots. Once a new generation is found that is better, the screen will pan back and restart the race."
echo "You can make them hop slower / faster by changing the --slow flag."

bin/sentinel-evolution --demo --seed 412 --jobs 32 --threads `nproc --all` --slow 2
