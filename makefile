CPPFILES:=$(wildcard src/*.cpp)
OFILES:=$(patsubst src/%.cpp,build/%.o,$(CPPFILES))

CXXFLAGS:=-std=c++23 -Wall -Wextra -pedantic -O3
LIBS:=-lSDL2

bin/sentinel-evolution:$(OFILES)
	@mkdir -p bin
	$(CXX) -o $@ $(CXXFLAGS) $(OFILES) $(LIBS)

$(OFILES):build/%.o:src/%.cpp $(wildcard src/*.h)
	@mkdir -p build
	$(CXX) -c -o $@ $(CXXFLAGS) $<
