# sentinel-evolution

This toy program is an experiment on how to apply genetic evolution to neural nets.
The neural nets that are subject to this evolution control the legs of a sentinel bot that is supposed to learn how to walk without being taught.
Selection pressure is exerted over how far individuals can travel before tipping over or failing to choose a sensible leg to move.

Different evaluation modes exist, the most visually interesting being `--demo`.

There is a script that runs the demo with sensible parameters and parallelisation:

````
$ ./demo.sh
````

## Notes

The bot and its task was inspired by [Doom 3's sentry bot](https://doomwiki.org/w/images/a/a4/SentryBot.jpg).

## Dependencies

There currently is no configure script or cmake file as this is a small experimental tool.
The only hard requirements are a C++23 standard compliant C++ compiler (g++ 13) and SDL2 (i.e. libsdl2-dev).

## Copyright

This software is released under the AGPL 3.0 (see LICENSE file).
