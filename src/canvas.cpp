#include "canvas.h"
#include "SDL2/SDL.h"

#include <thread>
#include <chrono>
#include <cmath>
#include <vector>

using namespace window;

Vector Matrix::operator*(Vector const vec) const {
  auto const x = vec[0];
  auto const y = vec[1];
  auto const xt = a[0][0]*x + a[0][1]*y + a[0][2];
  auto const yt = a[1][0]*x + a[1][1]*y + a[1][2];
  return {xt, yt};
}

Vector window::operator+(Vector const lhs, Vector const rhs) {
  return {lhs[0] + rhs[0], lhs[1] + rhs[1]};
}
Vector window::operator-(Vector const lhs, Vector const rhs) {
  return {lhs[0] - rhs[0], lhs[1] - rhs[1]};
}
int window::cross_product(Vector const lhs, Vector const rhs) {
  return lhs[0]*rhs[1] - lhs[1]*rhs[0];
}

struct Canvas::Impl {
  SDL_Renderer *renderer;
  SDL_Window *window;

  Matrix inputTransform{};

  struct Brushes {
    RGB background{255,255,255};
    RGB strokeBrush{0,0,0};
    RGB fillBrush{192,192,192};
  };
  std::vector<Brushes> brushes{Brushes{}};

  bool exitRequest{false};

  void setColor(Brush const brush, std::uint8_t const alpha = 255) {
    RGB c{255,0,0};
    if (brush == Brush::background) {
      c = brushes.back().background;
    } else if (brush == Brush::stroke) {
      c = brushes.back().strokeBrush;
    } else if (brush == Brush::fill) {
      c = brushes.back().fillBrush;
    }
    SDL_SetRenderDrawColor(renderer, c[0], c[1], c[2], alpha);
  }

  std::array<std::vector<SDL_Point>,2> pointBuf{};
};

Canvas::Canvas(std::string const& name) : pimpl{std::make_unique<Canvas::Impl>()} {
  SDL_Init(SDL_INIT_VIDEO);
  pimpl->window = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 2*1024, 2*768, /*SDL_WINDOW_FULLSCREEN_DESKTOP*/SDL_WINDOW_RESIZABLE);
  pimpl->renderer = SDL_CreateRenderer(pimpl->window, -1, 0);
  draw();
}
Vector Canvas::tr(Vector const pt) const {
  return pimpl->inputTransform*pt;
}

void Canvas::clear() {
  pimpl->setColor(Brush::background);
  SDL_RenderClear(pimpl->renderer);
}
bool Canvas::live() const {
  return !pimpl->exitRequest;
}
void Canvas::tick() {
  SDL_Event event;
  if (SDL_PollEvent(&event) && event.type == SDL_QUIT) {
    pimpl->exitRequest = true;
  }
  SDL_Delay(10);
}
void Canvas::blockWindow() {
  while (live()) {
    tick();
  }
}
Canvas::~Canvas() {
  SDL_DestroyRenderer(pimpl->renderer);
  SDL_DestroyWindow(pimpl->window);
  SDL_Quit();
}

Vector Canvas::size() const {
  int w{};
  int h{};
  SDL_GetWindowSize(pimpl->window, &w, &h);
  return {std::max(0, w), std::max(0, h)};
}

void Canvas::draw() {
  SDL_RenderPresent(pimpl->renderer);
  SDL_RenderClear(pimpl->renderer);

  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT)
      pimpl->exitRequest = true;
  }
}
void Canvas::pushBrushes() {
  pimpl->brushes.push_back(pimpl->brushes.back());
}
void Canvas::popBrushes() {
  if (pimpl->brushes.size() > 1)
    pimpl->brushes.pop_back();
}

void Canvas::setTransform(Matrix const matrix) {
  pimpl->inputTransform = matrix;
}


void Canvas::setColor(RGB const color, Brush const brush) {
  if (brush == Brush::background) {
    pimpl->brushes.back().background = color;
  } else if (brush == Brush::stroke) {
    pimpl->brushes.back().strokeBrush = color;
  } else if (brush == Brush::fill) {
    pimpl->brushes.back().fillBrush = color;
  }
}
void Canvas::drawLine(Vector from, Vector to, int thickness) {
  pimpl->setColor(Brush::stroke);
  from = tr(from);
  to = tr(to);
  if (thickness == 1) {
    SDL_RenderDrawLine(pimpl->renderer, from[0], from[1], to[0], to[1]);
  } else if (thickness > 1) {
    std::array<double,2> const orth = {static_cast<double>(from[1]-to[1]), static_cast<double>(to[0]-from[0])};
    double const scale = static_cast<double>(thickness)/2 / std::sqrt(orth[0]*orth[0] + orth[1]*orth[1]);
    Vector const norm{static_cast<int>(orth[0]*scale), static_cast<int>(orth[1]*scale)};
    pimpl->pointBuf[0].clear();
    auto const a = from + norm;
    auto const b = from - norm;
    auto const c = to + norm;
    auto const d = to - norm;
    auto const x0 = std::min({a[0],b[0],c[0],d[0]});
    auto const x1 = std::max({a[0],b[0],c[0],d[0]});
    auto const y0 = std::min({a[1],b[1],c[1],d[1]});
    auto const y1 = std::max({a[1],b[1],c[1],d[1]});
    for (auto x = x0; x <= x1; ++x) {
      for (auto y = y0; y <= y1; ++y) {
        Vector pt{x,y};
        if ((cross_product(d-b, pt-b) > 0) && (cross_product(c-a, pt-a) < 0) && (cross_product(a-b, pt-b) < 0) && (cross_product(c-d, pt-d) > 0)) {
          pimpl->pointBuf[0].push_back({pt[0],pt[1]});
        }
      }
    }
    SDL_RenderDrawPoints(pimpl->renderer, pimpl->pointBuf[0].data(), static_cast<int>(pimpl->pointBuf[0].size()));
  }
}
void Canvas::drawPoint(Vector pt) {
  pimpl->setColor(Brush::stroke);
  pt = tr(pt);
  SDL_RenderDrawPoint(pimpl->renderer, pt[0], pt[1]);
}
void Canvas::drawRect(Vector center, Vector const sz) {
  center = tr(center);
  SDL_Rect rect{
    center[0] - sz[0]/2,
    center[1] - sz[1]/2,
    sz[0],
    sz[1]
  };
  pimpl->setColor(Brush::fill);
  SDL_RenderFillRect(pimpl->renderer, &rect);
  pimpl->setColor(Brush::stroke);
  SDL_RenderDrawRect(pimpl->renderer, &rect);
}

void Canvas::drawEllipse(Vector center, Vector const sz) {
  pimpl->setColor(Brush::fill);
  center = tr(center);
  auto const rx = static_cast<double>(sz[0]) / 2.;
  auto const ry = static_cast<double>(sz[1]) / 2.;
  auto const rxSq = rx*rx;
  pimpl->pointBuf[0].clear();
  pimpl->pointBuf[1].clear();
  pimpl->setColor(Brush::fill);
  for (int x = -(sz[0] / 2.); x <= (sz[0] / 2.); ++x) {
    auto const xd = static_cast<double>(x);
    auto const y = static_cast<int>(ry * std::sqrt(1 - xd*xd/rxSq));
    SDL_RenderDrawLine(pimpl->renderer, center[0]+x, center[1]-y, center[0]+x, center[1]+y);
    pimpl->pointBuf[0].push_back({center[0]+x,center[1]-y});
    pimpl->pointBuf[1].push_back({center[0]+x,center[1]+y});
  }
  pimpl->setColor(Brush::stroke);
  SDL_RenderDrawLines(pimpl->renderer, pimpl->pointBuf[0].data(), static_cast<int>(pimpl->pointBuf[0].size()));
  SDL_RenderDrawLines(pimpl->renderer, pimpl->pointBuf[1].data(), static_cast<int>(pimpl->pointBuf[1].size()));
}

