#pragma once

#include <memory>
#include <array>

namespace window {

  using RGB = std::array<std::uint8_t, 3>;

  enum class Brush {
    background, stroke, fill
  };

  using Vector = std::array<int,2>;

  Vector operator+(Vector, Vector);
  Vector operator-(Vector, Vector);
  int operator*(Vector, Vector);
  int cross_product(Vector, Vector);

  struct Matrix {
    std::array<std::array<int,3>,3> a{{{1,0,0},{0,1,0},{0,0,1}}};
    Vector operator*(Vector const vec) const;
  };

  class Canvas {
    private:
      class Impl;
      std::unique_ptr<Impl> pimpl;
      Vector tr(Vector) const;
    public:
      Canvas(std::string const& = "Sentinel Evolution");
      bool live() const;
      void tick();
      void blockWindow();
      void clear();

      Vector size() const;

      void setTransform(Matrix = Matrix{});
      void setColor(RGB, Brush);
      void pushBrushes();
      void popBrushes();

      void drawLine(Vector, Vector, int thickness = 1);
      void drawPoint(Vector);
      void drawRect(Vector const center, Vector const sz);
      void drawEllipse(Vector const center, Vector const sz);

      void draw();

      template <typename Painter>
      void blockWindow(Painter const& painter) {
        while (live()) {
          painter();
          tick();
        }
      }

      ~Canvas();
  };

}
