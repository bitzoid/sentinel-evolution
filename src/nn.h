#pragma once

#include <array>
#include <iostream>
#include <iomanip>
#include <string>

template <std::size_t L0, std::size_t L1, std::size_t L2>
struct NeuralNet {
  std::array<double, L0*L1 + L1*L2> weights;


  std::array<double, L2> eval(std::array<double, L0> const input) const {
    std::array<double, L1> nodes1;
    std::array<double, L2> nodes2;
    nodes1.fill(0);
    nodes2.fill(0);

    std::size_t k = 0;
    for (std::size_t i0 = 0; i0 < L0; ++i0) {
      for (std::size_t i1 = 0; i1 < L1; ++i1) {
        nodes1[i1] += input[i0]*weights[k++];
      }
    }
    for (std::size_t i1 = 0; i1 < L1; ++i1) {
      for (std::size_t i2 = 0; i2 < L2; ++i2) {
        nodes2[i2] += nodes1[i1]*weights[k++];
      }
    }
    return nodes2;
  }

  friend std::ostream& operator<<(std::ostream& os, NeuralNet const& nn) {
    os << "{";
    std::string sep = "";
    for (auto const& d: nn.weights) {
      os << sep;
      if (d >= +0)
        os << '+';
      os << std::setw(3) << std::fixed << d;
      sep = ", ";
    }
    os << "}";
    return os;
  }
};
