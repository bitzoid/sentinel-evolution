#include "pop.h"

#include <algorithm>
#include <stdexcept>

#include <iostream>

void Pop::resetNet(Pop::Net& net) {
  std::uniform_real_distribution<double> m(-1.,+1.);
  for (auto& w: net.weights) {
    w = gen(m);
  }
}

void Pop::resetNet(std::size_t const i) {
  return resetNet(pops[i].net);
}

void Pop::resetBot(std::size_t const i) {
  auto& unit = pops[i];
  unit.bot.reset();
  unit.score = 0;
  unit.live = true;
}

void Pop::init(Unit& unit) {
  unit.bot.reset();
  resetNet(unit.net);
  unit.score = 0;
  unit.live = true;
  unit.id = ++nextId;
}

void Pop::init(std::size_t const num) {
  pops.resize(num);
  for (auto& unit: pops) {
    init(unit);
  }
}

bool Pop::puppeteer(Bot& bot, Net const& nn) {
  auto const res = nn.eval(
    std::array<double, 8>{
      bot.legs[0].angle,
      bot.legs[0].extension,
      bot.legs[1].angle,
      bot.legs[1].extension,
      bot.legs[2].angle,
      bot.legs[2].extension,
      bot.legs[3].angle,
      bot.legs[3].extension,
    }
  );
  std::size_t const front = res[0] > 0;
  std::size_t const right = res[1] > 0;
  std::size_t const lookup[] = {2,1,3,0};
  std::size_t const legId = lookup[(front << 1) | right];
  bot.legs[legId].status = Leg::Status::lifted;
  bot.legs[legId].angle = std::clamp(res[2], Leg::angleRange.min, Leg::angleRange.max);
  bot.legs[legId].extension = std::clamp(res[3], Leg::extensionRange.min, Leg::extensionRange.max);
  //std::cout << "move leg " << legId << " " << bot.legs[legId].angle << "," << bot.legs[legId].extension << "\n";
  return bot.stable();
}

void Pop::mutate(Net& net) {
  std::uniform_int_distribution<std::size_t> w(0,net.weights.size()-1);
  //std::uniform_real_distribution<double> m(-1.,+1.);
  std::exponential_distribution<double> m(1.6);
  std::exponential_distribution<double> exp(0.3);
  std::uniform_int_distribution<int> sign(0,1);
  auto const num = static_cast<std::size_t>(std::ceil(gen(exp)));
  for (std::size_t i = 0; i < num; ++i) {
    std::size_t const idx = gen(w);
    double diff = gen(m);
    if (gen(sign)) diff = -diff;
    net.weights[idx] += diff;
  }
}
void Pop::mutate(Unit& unit) {
  mutate(unit.net);
  unit.age = 1;
}

double Pop::botScore(Bot const& bot) const {
  auto score = norm(bot.head);
  auto const lps = bot.legPositions();
  score += (lps[0][1] + lps[1][1] + lps[2][1] + lps[3][1])/16;
  return score;
}

std::size_t Pop::moveFeet() {
  std::size_t lives = 0;
  for (auto& unit: pops) {
    if (!unit.live) continue;
    for (std::size_t i = 0; i < 4; ++i) {
      unit.bot.legs[i].status = Leg::Status::fixed;
    }
    if ((unit.live = puppeteer(unit.bot, unit.net))) {
      //unit.score = norm(unit.bot.head);
      //auto const lps = unit.bot.legPositions();
      //unit.score += (lps[0][1] + lps[1][1] + lps[2][1] + lps[3][1])/16;
      ++lives;
    }
  }
  return lives;
}

void Pop::stomp() {
  for (auto& unit: pops) {
    if (!unit.live) continue;
    unit.bot.stomp();
  }
}

void Pop::moveHead() {
  for (auto& unit: pops) {
    if (!unit.live) continue;
    unit.bot.headForward();
    //unit.score = norm(unit.bot.head);
    unit.score = botScore(unit.bot);
  }
}

void Pop::reap() {
  std::stable_sort(begin(pops), end(pops), [](Unit const& lhs, Unit const& rhs) {
    return lhs.score > rhs.score;
    //if (lhs.score-rhs.score > 1e-6) return true;
    //if (lhs.score-rhs.score < -1e-6) return false;
    //else return (lhs.age < rhs.age);
  });
  std::size_t const keep = std::max(std::size_t{1}, pops.size()/4);
  pops.resize(keep);
  if (keep > 3) {
    init(pops.back());
  }
  for (std::size_t i = 0; i < pops.size(); ++i) {
    resetBot(i);
  }
  for (std::size_t i = 0; i < keep; ++i) {
    pops[i].age++;
    for (std::size_t k = 0; k < 3; ++k) {
      pops.push_back(pops[i]);
      mutate(pops.back());
      pops.back().id = ++nextId;
    }
  }
  //std::reverse(begin(pops) + 2, end(pops));
}
