#pragma once

#include "nn.h"
#include "sentinel.h"

#include <variant>
#include <vector>
#include <tuple>
#include <random>
#include <memory>

class Pop {
  public:
    using Net = NeuralNet<8,7,4>;
    using Score = double;
    using Live = bool;
    using UId = std::uint64_t;
    struct Unit {
      Bot bot;
      Net net;
      Score score;
      Live live;
      unsigned age = 0;
      UId id = 0;
    };
  private:
    std::variant<std::unique_ptr<std::random_device>,std::unique_ptr<std::mt19937>,std::unique_ptr<std::ranlux24_base>> gen_;
    template <typename Dis>
    auto gen(Dis& dis) {
      return std::visit([&dis](auto const& gp) { return dis(*gp); }, gen_);
    }
    std::vector<Unit> pops{};

    UId nextId = 0;

    static bool puppeteer(Bot& bot, Net const& nn);
    void mutate(Net&);
    void mutate(Unit&);
    void resetNet(Net&);
    void resetNet(std::size_t);
    void init(std::size_t);
    void init(Unit&);
    double botScore(Bot const&) const;

    static decltype(gen_) mkGen(unsigned const seed) {
      if (seed == 0) {
        return std::make_unique<std::random_device>();
      } else if (seed < 1000) {
        return std::make_unique<std::mt19937>(seed);
      } else {
        return std::make_unique<std::ranlux24_base>(seed);
      }
    }
  public:
    Pop(std::size_t const num, unsigned const seed = 161) : gen_{mkGen(seed)} {
      init(num);
    }
    std::size_t size() const noexcept {
      return pops.size();
    }
    auto const& operator[](std::size_t const i) const {
      return pops[i];
    }
    void resetBot(std::size_t);
    auto& override(std::size_t const i) {
      return pops[i];
    }
    void overrideNet(std::size_t const i, Net const& net) {
      pops[i].net = net;
      resetBot(i);
    }
    void assasinate(std::size_t const i) {
      init(i);
    }
    void addPop() {
      pops.emplace_back();
      init(pops.back());
    }
    std::size_t moveFeet();
    void stomp();
    void moveHead();
    void reap();
};
