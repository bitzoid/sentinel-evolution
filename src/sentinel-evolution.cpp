#include "sentinel.h"
#include "canvas.h"
#include "pop.h"

#include <concepts>
#include <iostream>
#include <thread>
#include <atomic>
#include <optional>
#include <algorithm>

using window::Brush;
using window::Canvas;
using Pt = window::Vector;
using window::operator+;
using window::operator-;

namespace {
  struct Config {
    unsigned seed = 161;
    unsigned jobs = 8;
    unsigned slow = 1;
    unsigned threads = 0;
  };

  struct Painter {
    std::unique_ptr<Canvas> canvas;
    std::ostream& out;
    bool silent = false;
    constexpr static const double scale = 100;

    void drawBody(Bot const& bot, Pt const offset) {
      if (silent || !canvas) return;

      canvas->pushBrushes();
      Pt const from{static_cast<int>(scale*bot.head.xy[0]), static_cast<int>(scale*bot.head.xy[1])};
      auto const drawLegs = [&]() {
        canvas->setColor({40,128,80}, Brush::stroke);
        std::size_t legIdx = 0;
        for (auto const [xy]: bot.legPositions()) {
          Pt const to{static_cast<int>(scale*xy[0]), static_cast<int>(scale*xy[1])};
          switch (bot.legs[legIdx].status) {
            case Leg::Status::fixed: {
              canvas->setColor({40,128,80}, Brush::stroke);
            } break;
            case Leg::Status::lifted: {
              canvas->setColor({128,80,40}, Brush::stroke);
            } break;
          }
          canvas->drawLine(offset+from, offset+to, 4);
          legIdx++;
        }
      };
      auto const drawTorso = [&]() {
        if (bot.stable()) {
          canvas->setColor({64,200,128}, Brush::fill);
        } else {
          canvas->setColor({200,64,128}, Brush::fill);
        }
        canvas->setColor({40,128,80}, Brush::stroke);
        canvas->drawEllipse(offset+from, {40,40});
        canvas->setColor({240,240,240}, Brush::fill);
        canvas->setColor({20,20,20}, Brush::stroke);
        auto const eyes = Point::fromPolar(bot.angle, 16);
        auto const eyesN = Point::fromPolar(bot.angle+0.25, 7);
        Pt const eyeL{static_cast<int>(eyes[0]+eyesN[0]), static_cast<int>(eyes[1]+eyesN[1])};
        Pt const eyeR{static_cast<int>(eyes[0]-eyesN[0]), static_cast<int>(eyes[1]-eyesN[1])};
        canvas->drawEllipse(offset+from+eyeL, {7,7});
        canvas->drawEllipse(offset+from+eyeR, {7,7});
      };
      drawTorso();
      drawLegs();
      canvas->popBrushes();
    };

    int verticalPan = 0;

    template <typename P>
    window::Vector drawPops(P const& population, int const offset, bool const doPan = false) {
      if (!canvas) return window::Vector{0,0};
      auto const sz = canvas->size();
      if (silent) return sz;
      canvas->setTransform(window::Matrix{{{
          {1,0,0*static_cast<int>(sz[0])/2}
        , {0,-1,static_cast<int>(verticalPan + sz[1])}
        , {0,0,1}
      }}});
      canvas->clear();
      canvas->drawLine({0, offset}, {sz[0], offset});
      for (std::size_t i = 0; i < population.size(); ++i) {
        if constexpr (std::is_same_v<P, Pop>) {
          drawBody(population[i].bot, {static_cast<int>(100+i*150),offset});
        } else {
          drawBody(population[i], {static_cast<int>(100+i*150),offset});
        }
      }
      canvas->draw();

      if (doPan) {
        for (std::size_t i = 0; i < population.size(); ++i) {
          double y = scale;
          if constexpr (std::is_same_v<P, Pop>) {
            y += population[i].bot.head[1] * scale;
          } else {
            y += population[i].head[1] * scale;
          }
          if (y + 0.5*scale > verticalPan + sz[1]) {
            verticalPan += 0.8*sz[1];
          }
        }
      }
      return sz;
    }

  };

  struct Engineer {
    void onTick(Painter&);
  };

  struct Sandbox : Engineer {
    std::size_t frame = 0;
    Bot bot;
    Bot bot2;
    Bot bot3;

    Sandbox(Config const&) {
      bot.legs[0].angle = 0.4;
      bot.legs[1].angle = 0.7;
      bot.legs[2].angle = 0.5;
      bot.legs[3].angle = 0.5;
      bot.legs[0].extension = bot.legs[3].extension = 1;
      bot.legs[1].extension = bot.legs[2].extension = 0.3;
      bot.legs[3].status = Leg::Status::lifted;
      bot.maxLeg = 1;

      bot2 = bot;
      bot3 = bot;

      bot2.legs[3].status = Leg::Status::fixed;
    }

    void onTick(Painter& painter) {
      ++frame;
      if (!painter.canvas) return;
      auto const sz = painter.canvas->size();
      painter.canvas->setTransform(window::Matrix{{{{1,0,static_cast<int>(sz[0])/2},{0,-1,static_cast<int>(sz[1])},{0,0,1}}}});
      painter.canvas->clear();
      int const offset = 100;
      painter.canvas->drawLine({-sz[0]/2, offset}, {+sz[0]/2, offset});
      painter.drawBody(bot, {0,offset});
      painter.drawBody(bot2, {150,offset});
      painter.drawBody(bot3, {300,offset});
      bot3.legs[0].angle += 0.01;
      if (bot3.legs[0].angle > 1) {
        bot3.legs[0].angle = 0;
        if (bot3.legs[3].status == Leg::Status::fixed)
          bot3.legs[3].status = Leg::Status::lifted;
        else if (bot3.legs[3].status == Leg::Status::lifted)
          bot3.legs[3].status = Leg::Status::fixed;
      }

      if (frame == 100) {
        bot.headForward();
        bot2.headForward();
        bot3.headForward();
      }
      if (frame == 200 || frame == 400) {
        bot2.legs[0].angle = 0.3;
        bot2.legs[1].angle = 0.3;
        bot2.legs[2].angle = 0.7;
        bot2.legs[3].angle = 0.8;
        bot2.legs[0].extension = 0.8;
        bot2.legs[1].extension = 0.5;
        bot2.legs[2].extension = 0.5;
        bot2.legs[3].extension = 0.8;
      }
      if (frame == 300 || frame == 500) {
        bot2.headForward();
      }
      painter.canvas->draw();
    }
  };

  struct Evolution : Engineer {
    Pop population;
    std::size_t frame = 0;
    double lastSumScore = 0;
    unsigned gen = 0;
    unsigned stuck = 0;
    double peakScore = 0;

    constexpr static const std::size_t Num = 40;

    Evolution(Config const& config)
      : population(Num, config.seed) {
    }

    void onTick(Painter& painter) {
      ++frame;

      auto const moveFeet = [&]() {
        population.moveFeet();
      };
      auto const stomp = [&]() {
        population.stomp();
      };
      auto const moveHead = [&]() {
        population.moveHead();
      };
      auto const reap = [&](bool last = false) {
        auto const dumpPops = [&]() {
          painter.out << "[F" << frame << "/" << last << "]  pops:\t";
          for (std::size_t j = 0; j < population.size(); ++j) {
            painter.out << "(#" << population[j].id << ", " << population[j].score  << ", " << population[j].age << ")  ";
          }
          painter.out << "\n\n";
        };
        double sumScore = 0;
        for (std::size_t i = 0; i < population.size(); ++i) {
          sumScore += population[i].score;
          if (population[i].score > peakScore) {
            peakScore = population[i].score;
            painter.out << "New peak score: " << peakScore << "\t(Frame: " << frame << ", slot: " << i << ", age: " << population[i].age << ") -- NeuralNet:\n";
            painter.out << "\t" << population[i].net << "\n";
            dumpPops();
          }
        }
        if ((sumScore <= lastSumScore+1e-3)) {
          ++stuck;
        } else {
          stuck = 0;
        }
        if (stuck > 4) {
          //painter.out << "~~~~~~~~~~~~~~ stuck reap at frame " << frame << " with sumScore: " << sumScore << " and lastSumScore: " << lastSumScore << "\n";
          last = true;
        }
        if (gen > 2000) {
          //painter.out << "~~~~~~~~~~~~~~ force reap at frame " << frame << " with sumScore: " << sumScore << " and lastSumScore: " << lastSumScore << "\n";
          last = true;
        }
        //dumpPops();
        if (last) {
          painter.drawPops(population, 100, false);
          if ((frame % 1000) == 0) {
            double highest = 0;
            for (std::size_t i = 0; i < population.size(); ++i) {
              highest = std::max(highest, population[i].score);
            }
            painter.out << "Frame: " << frame << " with current high score: " << highest << ". Stopping after " << gen << " steps.\n";
            //painter.out << population[population.size()-1].net << "\n";
            if (highest+1e-6 < peakScore) {
              dumpPops();
              painter.out << "Warning: High score (" << highest << ") is below peakScore: " << peakScore << "\n";
            }
          }
        }
        if (last) {
          population.reap();
          gen = 0;
          lastSumScore = 0;
          stuck = 0;
        } else {
          ++gen;
          lastSumScore = sumScore;
        }
        return last;
      };
      for (int i = 100; i--; ) {
        moveFeet();
        stomp();
        moveHead();
        if (reap(i==0)) break;
      }

    }
  };

  struct Parallel : Engineer {
    std::vector<Pop> pops{};
    std::vector<unsigned> popseeds{};
    std::vector<std::thread> threads{};
    std::vector<Bot> bots{};
    unsigned frame = 0;
    std::size_t threadNum = 1;

    constexpr static const std::size_t Num = 40;

    Parallel(Config const& config) : threadNum{config.threads?config.threads:config.jobs} {
      unsigned lb = 0;
      unsigned ub = 0;
      if (config.seed == 0) {
      } else if (config.seed < 1000) {
        lb = 1;
        ub = 900;
      } else {
        lb = 1000;
        ub = 65536;
      }
      std::mt19937 gen(config.seed);
      std::uniform_int_distribution<unsigned> seeds(lb,ub);
      pops.reserve(Num);
      for (unsigned i = 0; i < config.jobs; ++i) {
        popseeds.push_back(seeds(gen));
      }
      if (config.seed != 0) {
        std::sort(begin(popseeds), end(popseeds));
        popseeds.erase(std::unique(begin(popseeds), end(popseeds)), end(popseeds));
        while ((popseeds.size() < config.jobs) && (popseeds.back() < ub)) {
          popseeds.push_back(popseeds.back()+1);
        }
      }
      for (auto const s: popseeds) {
        pops.emplace_back(Num, s);
      }
    }
    void onTick(Painter& painter) {
      if (frame == 0) {
        for (std::size_t i = 0; i < popseeds.size(); ++i) {
          painter.out << "Population [" << i << "] with seed " << popseeds[i] << "\n";
        }
      }
      ++frame;
      threads.clear();
      bots.clear();
      threads.reserve(threadNum);
      bots.reserve(size(pops));
      auto const worker = [&](std::size_t const i) {
        auto const walz = [&](std::size_t const reps) {
          for (std::size_t j = 0; j < reps; ++j) {
            pops[i].moveFeet();
            pops[i].stomp();
            pops[i].moveHead();
          }
        };
        for (std::size_t k = 0; k < 20; ++k) {
          walz(100);
          pops[i].reap();
        }
        if (pops[i][0].score > 10) {
          walz(static_cast<std::size_t>(pops[i][0].score * pops[i][0].score * 10));
        }
        for (std::size_t h = 0; h < pops[i].size(); ++h) {
          if (!pops[i][h].live) {
            pops[i].assasinate(h);
          }
        }
        pops[i].reap();
        walz(100);
      };
      auto const multiworker = [&](std::size_t const i) {
        for (auto j = i; j < pops.size(); j += threads.size()) {
          worker(j);
        }
      };
      for (auto const& pop: pops) {
        bots.push_back(pop[0].bot);
      }
      for (std::size_t i = 0; i < threadNum; ++i) {
        threads.emplace_back(multiworker, i);
      }
      painter.drawPops(bots, 100, true);
      for (auto& thread: threads) {
        thread.join();
      }
      if ((frame%100) == 0) {
        std::size_t best = 0;
        std::size_t worst = 0;
        for (std::size_t j = 0; j < pops.size(); ++j) {
          if (pops[j][0].score > pops[best][0].score) {
            best = j;
          }
          if (pops[j][0].score < pops[worst][0].score) {
            worst = j;
          }
        }
        painter.out << "Colony cross-seeding from [" << best << "]/" << popseeds[best] << " to [" << worst << "]/" << popseeds[worst] << " and score " << pops[best][0].score << "\n\t" << pops[best][0].net << "\n" << std::flush;
        pops[worst].override(0) = pops[best][0];
      }
    }
  };

  struct Replay : Engineer {
    std::array<Pop::Net,8> nets{};
    Pop population;
    std::size_t frame = 0;
    unsigned inc = 0;
    unsigned slow = 1;

    Replay(Config const& config) : population(nets.size()), slow(config.slow) {
      //nets[0] = {{+2.871191, +2.020507, -1.055188, -0.385024, +2.637573, -0.279017, -1.439960, +1.873420, -4.917893, -0.633179, +1.106903, -1.435299, +1.079657, +1.895509, +1.894033, +2.321954, -1.127627, -1.770166, -1.806090, +1.115189, -2.655126, -2.224026, +0.744561, +1.308888, +1.104943, -1.384838, +0.935568, -2.097399, +1.841896, +2.338588, -2.717471, +0.993943, +2.395913, +3.029479, -0.735945, +0.807691, +1.735226, +2.596159, -0.502173, -0.093954, -2.614271, +2.774219, -1.687520, -2.133542, +1.382090, +0.676177, -0.211251, -4.122101}};
      //nets[1] = {{+2.711458, +0.483863, -0.046848, -0.817415, +2.049914, -0.370291, -1.328453, +1.507555, -1.328343, -1.220534, +1.709629, -0.271360, +0.594880, +1.127246, +1.941746, +0.323521, -1.177771, -1.867360, -0.502284, +0.024967, -2.001127, -1.148551, -1.510564, +0.401818, +0.621613, +2.003981, +0.935568, -1.687631, -0.325859, +0.699601, -2.447192, -0.356938, +1.038895, +1.573883, -0.000901, +0.303999, -0.575989, +3.009056, -0.480696, -0.613609, -1.805124, +0.496859, -1.175726, -1.688505, -0.331663, +1.086789, -0.072370, -3.540116}};
      //nets[2] = {{+2.871191, +2.020507, -1.055188, -0.385024, +2.637573, -0.279017, -1.439960, +1.873420, -4.917893, -0.633179, +1.106903, -1.435299, +1.079657, +1.895509, +1.894033, +2.321954, -1.127627, -1.770166, -1.806090, +1.115189, -2.655126, -2.224026, +0.744561, +1.308888, +1.104943, -1.384838, +0.935568, -2.097399, +1.841896, +2.338588, -2.717471, +0.993943, +2.395913, +3.029479, -0.735945, +0.807691, +1.735226, +2.596159, -0.502173, -0.093954, -2.614271, +2.774219, -1.687520, -2.133542, +1.382090, +0.676177, -0.211251, -4.122101}};
      nets[1] = {{-0.794305, +0.544343, +1.877719, -0.406406, +0.833192, -1.197486, +1.008874, +0.443792, -0.290613, -0.154376, -1.067690, +0.228841, +0.499175, +0.112237, +0.137355, -1.668878, +0.371618, +1.350886, -3.316005, +0.606589, -0.751086, +0.893687, +1.199512, -0.263822, -0.243275, -1.331796, +1.113562, -0.179050, -1.334316, +0.880312, -0.609975, -0.061655, +0.949195, +0.772425, -0.525380, +0.282620, +0.949925, -1.005009, +2.608747, +1.020497, +0.355434, +0.583601, -0.997319, -0.357977, +0.393015, -1.896209, +0.520202, -0.885842, +0.380189, +0.159308, +0.599300, +0.565455, -0.751625, +0.124326, +0.196692, -1.680856, -2.384600, +0.117304, +0.795833, -0.145511, -0.784983, +3.411917, -0.757887, +1.232040, -1.047587, +6.858376, +0.903715, -0.726070, -0.126386, -1.348511, +0.921676, -0.294866, +0.703911, -0.213902, -0.426491, +1.263982, +0.876657, -1.804463, +0.540365, +0.272295, -1.584026, -1.876131, -0.611290, -0.145658}};
      nets[2] = {{-0.803431, +0.545058, +1.877719, -0.437081, +0.833192, -1.198308, +1.008874, +0.443792, -0.290619, -0.153143, -1.066827, +0.228841, +0.499175, +0.133968, +0.137221, -1.668878, +0.371618, +1.350886, -3.316005, +0.606584, -0.751086, +0.893687, +1.199512, -0.263882, -0.243275, -1.331796, +1.117430, -0.179050, -1.334316, +0.880312, -0.609975, -0.043072, +0.949195, +0.772425, -0.525380, +0.282645, +0.957039, -1.005009, +2.608747, +1.020497, +0.355434, +0.583601, -0.997319, -0.357977, +0.382622, -1.896209, +0.523869, -0.885406, +0.372442, +0.185401, +0.599300, +0.565455, -0.751351, +0.124326, +0.196692, -1.681780, -2.440730, -1.130356, +0.795833, -0.145511, -0.784983, +2.151456, -0.757887, +1.232040, -1.047587, +5.525396, +0.903715, -0.619257, -0.126386, -4.025861, +0.921763, -0.294866, +0.703911, +0.845324, -0.426491, +1.267420, +0.876657, -1.433381, +0.540365, +0.272295, -1.601889, -3.979310, -0.611290, -0.145609}};
      nets[3] = {{-1.329865, +3.998142, +0.616183, +0.771903, +0.300882, -0.207583, -0.527697, -0.794842, +0.805448, +0.340128, +1.167232, +1.299722, -0.533946, +1.190385, -0.439009, -0.106870, -0.726148, -0.575567, -0.291574, +0.214163, -0.458810, +0.158004, -2.134640, +0.001974, +0.320275, +0.031667, -3.428723, -0.434536, -0.641702, -0.139463, +0.461931, +1.650335, +0.832031, +0.074729, -1.241789, -0.333936, +0.493604, -1.994240, -0.839936, -0.216544, -0.990479, +0.157254, -0.542484, +0.562006, +0.870881, -0.839411, -0.428401, +0.242060, -0.318848, -1.017275, -0.038708, -0.109795, +0.034900, +1.153429, +0.735964, -0.487837, +2.389112, -0.157317, +1.045456, -0.020622, +0.961142, +0.818555, -1.975140, +0.459109, -0.634391, +1.749820, -7.786497, +1.080914, +2.209244, -3.066903, -0.492369, +0.212238, +3.144405, +3.469694, -0.012379, +0.994808, -0.118613, +1.279573, -0.329090, +0.027972, -0.295599, -1.540998, +1.368261, +1.413754}};
      nets[4] = {{+0.116365, +2.087330, -0.535121, +1.433034, +1.134315, -2.059479, +0.925853, -0.005401, +0.017039, +0.802317, -0.108963, -1.128192, -0.685768, -1.035364, +0.808394, -0.993877, -0.515127, +2.722556, +0.410906, +0.415167, -0.564461, -0.677390, +2.026848, +2.466140, -2.497566, +1.996818, +2.855124, +0.279859, -0.770772, -0.750674, -1.901179, -0.232072, +1.833434, -0.230031, -0.516673, -0.389505, -1.982877, +0.002550, +0.120409, -0.246112, -0.594829, +1.031598, +0.167477, +0.452303, -0.607847, -0.961156, -0.895988, +0.076747, -0.440692, -0.965471, -1.480279, +0.276829, +0.737376, -0.386720, +1.576923, +0.555018, -5.042192, +3.540631, +0.626124, -0.860543, +2.091497, +2.276806, -5.678539, +0.027048, -5.420170, -0.103768, +3.219158, -0.287954, +1.014221, -0.006646, +0.110805, +0.182300, -1.583484, +1.475922, +1.708507, +0.020550, +0.509056, -0.576730, -0.385382, -0.375658, -2.474834, -1.222919, -1.011279, -0.272008}};
      nets[5] = {{+0.058813, +0.184053, +1.827738, +0.608715, +0.651791, -1.737978, +2.119852, +1.388029, +1.155093, +3.876280, +0.061088, -0.687127, -1.567931, -1.295497, +0.570465, -0.810973, -0.368808, +0.357782, +0.330065, +0.569419, -0.903529, +0.496364, -0.785238, -1.390732, -0.822283, -1.847665, +1.414043, -0.094344, +1.453783, -0.233804, +1.136859, +0.000632, +0.042172, +0.889315, -0.015682, -0.902391, +0.658074, +0.236473, +0.267145, +1.276225, -0.699495, -1.263660, -2.497818, +0.660921, -0.257477, +0.232433, +0.815762, -2.668019, +0.543268, +2.388985, +0.599170, -1.643815, -1.032179, +0.199313, -1.388653, -1.186309, +0.085659, -2.602862, +0.184620, +0.604577, +0.539698, +2.607255, -0.623310, +3.154279, +0.446919, -0.919919, -1.011141, +0.130208, -0.552183, +1.793635, -0.885177, +1.490652, -0.293690, +2.003942, +0.135863, +2.454496, +0.147203, -1.976124, +2.117696, +1.423443, +0.079383, +0.710723, -1.602805, +0.091581}};
      nets[6] = {{+3.474507, -1.365842, +0.393414, +0.446932, -0.232395, -0.864991, -0.349068, -0.749338, -0.351709, -1.042305, +1.071748, +0.227571, +1.286417, -0.455585, +2.148478, +0.088555, +0.086933, -0.571850, -0.284447, -0.875416, -1.310512, -0.294949, +2.417183, +0.023883, -1.403138, -0.485931, -0.919463, +1.354453, -0.719230, +0.310048, +1.245910, +0.949659, +1.401225, +0.802966, -2.434517, +0.001910, +0.318960, -0.760454, -0.293305, +0.232212, +0.517648, -2.534429, -0.072218, -0.532342, +0.203646, -2.669237, +0.237870, -1.829080, -0.320640, +1.386328, -0.515045, -0.211940, +1.847304, -0.957660, +2.506008, +1.325509, +0.823191, +0.193447, -0.118825, +0.147588, +0.174343, -2.352332, +3.379906, -0.418707, +2.564423, -0.781717, +2.459198, +0.394886, +0.088332, -1.476944, +4.945435, -0.162049, +0.740460, +1.617392, -0.796218, +0.829635, +1.601204, -2.461086, +2.368518, +0.330390, +0.133392, +1.462452, -1.001673, -0.026518}}; //seed 718
      nets[7] = {{-0.764805, -0.300405, -0.615877, -0.885826, +0.010898, -0.601198, +1.293192, -1.167311, -0.628145, -0.273683, -1.229902, +1.266014, -0.950633, -1.854881, -0.770532, +0.628616, +1.945576, +1.276209, -0.526769, +1.039252, -1.140457, -0.060348, +0.353609, -0.661160, +0.753208, +1.056440, -0.817248, +1.279340, +0.705090, +0.797504, +0.405723, -0.884529, +1.730348, +1.090244, -1.660319, -0.313077, +0.461033, +0.805243, +0.354327, -0.337658, +4.557801, +1.202967, +0.921069, -1.282614, +0.468181, -0.879389, -0.949507, -3.066621, -1.139390, +0.495953, +1.012447, -0.521343, +0.151672, -1.767516, -1.165469, +1.526948, +0.719586, +1.477811, -0.832956, -0.335818, +3.006444, +13.202768, +1.008376, +0.324643, -8.028892, -11.686407, -0.227330, +0.018562, -11.601135, -0.245570, +0.406256, -0.820627, +9.623704, -13.327376, +1.537636, +0.409250, +1.049885, -8.122763, +0.113592, -0.030006, -2.564433, -10.229535, +0.174192, -0.035011}}; // mixed seeds
      for (std::size_t i = 0; i < nets.size(); ++i) {
        population.overrideNet(i, nets[i]);
      }
    }
    void onTick(Painter& painter) {
      ++frame;
      if ((frame%std::max(1u,slow)) == 0) {
        switch (inc++%3) {
          case 0: {
            population.moveFeet();
          } break;
          case 1: {
            population.stomp();
          } break;
          case 2: {
            population.moveHead();
          } break;
        }
        painter.drawPops(population, 100, true);
      }
    }
  };

  struct Demo : Engineer {
    Parallel computer;
    Pop pop;
    double peakScore = 0;
    unsigned frame = 0;
    unsigned inc = 0;
    unsigned slow = 1;

    std::optional<std::thread> worker{};
    std::atomic_flag working{};
    std::vector<Pop::Unit> results{};

    Demo(Config const& config) : computer{config}, pop{1}, slow{config.slow} {}
    void onTick(Painter& painter) {
      if (working.test() == false) {
        if (worker)
          worker->join();
        worker.reset();
        {
          std::size_t best = 0;
          for (std::size_t i = 0; i < computer.pops.size(); ++i) {
            if (computer.pops[i][0].score > computer.pops[best][0].score) {
              best = i;
            }
          }
          if (computer.pops[best][0].score > peakScore + ((peakScore>22.)?0.05:1)) {
            peakScore = computer.pops[best][0].score;
            painter.out << "new peak score: " << peakScore << '\n';
            results.push_back(computer.pops[best][0]);
          }
        }
        working.test_and_set();
        worker.emplace([&](){
          std::ostringstream buf;
          Painter devnull{nullptr,buf,true};
          computer.onTick(devnull);
          working.clear();
        });
      }

      if (inc == 0) {
        for (auto const& result: results) {
          pop.addPop();
          pop.override(pop.size()-1) = result;
          pop.resetBot(pop.size()-1);
          if (painter.verticalPan > 0) {
            for (std::size_t i = 0; i < pop.size(); ++i) {
              pop.resetBot(i);
            }
            painter.verticalPan = 0;
          }
        }
        results.clear();
      }

      ++frame;
      painter.drawPops(pop,100, true);
      if ((frame%std::max(1u,slow)) == 0) {
        switch (inc++) {
          case 0: {
            pop.moveFeet();
          } break;
          case 1: {
            pop.stomp();
          } break;
          case 2: {
            pop.moveHead();
          } [[fallthrough]];
          default:{
            inc = 0;
          }
        }
      }
    }
  };

  template <std::derived_from<Engineer> EngineerImpl>
  void run(std::unique_ptr<Canvas> canvas, Config const& config) {
    Painter painter{std::move(canvas), std::cout};
    EngineerImpl engineer{config};

    if (painter.canvas) {
      painter.canvas->blockWindow([&]() {
        engineer.onTick(painter);
      });
    } else {
      while (true) engineer.onTick(painter);
    }
  }
}

int main(int const argc, char const* const* const argv) {
  enum class Eng {
    sandbox, evolution, parallel, replay, demo
  };
  using namespace std::string_literals;

  Eng eng = Eng::evolution;
  Config config{};
  bool headless = false;

  for (int i = 1; i < argc; ++i) {
    if (argv[i] == "--sandbox"s) {
      eng = Eng::sandbox;
    } else if (argv[i] == "--evolution"s) {
      eng = Eng::evolution;
    } else if (argv[i] == "--parallel"s) {
      eng = Eng::parallel;
    } else if (argv[i] == "--replay"s) {
      eng = Eng::replay;
    } else if (argv[i] == "--demo"s) {
      eng = Eng::demo;
    } else if (argv[i] == "--headless"s) {
      headless = true;
    } else if (argv[i] == "--seed"s) {
      if (i+1 < argc) {
        config.seed = std::stoul(argv[++i]);
      } else {
        throw std::invalid_argument("--seed needs an argument.");
      }
    } else if (argv[i] == "--jobs"s) {
      if (i+1 < argc) {
        config.jobs = std::stoul(argv[++i]);
      } else {
        throw std::invalid_argument("--jobs needs an argument.");
      }
    } else if (argv[i] == "--threads"s) {
      if (i+1 < argc) {
        config.threads = std::stoul(argv[++i]);
      } else {
        throw std::invalid_argument("--threads needs an argument.");
      }
    } else if (argv[i] == "--slow"s) {
      if (i+1 < argc) {
        config.slow = std::stoul(argv[++i]);
      } else {
        throw std::invalid_argument("--slow needs an argument.");
      }
    } else if (argv[i] == "--help"s) {
      std::cout << argv[0] << "\n";
      std::cout << "\tEvolves neural net that governs walking for a four-legged bot ('sentinel').\n";
      std::cout << "Primary modes:\n";
      std::cout << "\t--evolution      Default mode. Evolves a population of " << Evolution::Num << " bots, selecting for travel distance and speed. Should converge in minutes. The max number of steps is capped." << "\n";
      std::cout << "\t--replay         Displays a pre-selected set of bots that were previously selected. They will walk as along as they don't tumble. The screen will pan to keep up." << "\n";
      std::cout << "\t--parallel       Runs several evolution enclaves in parallel. Occasinally there is some exchange between the enclaves from the best to the worst." << "\n";
      std::cout << "\t--demo           Runs evolution in background and shows all milestone evolutionary steps. This shows the progression of the neural net." << "\n";
      std::cout << "Additional flags:\n";
      std::cout << "\t--seed UINT      Choose a seed to be used. The default seed is 161. Seed 0 will use the real randomness of your hardware and may be very slow and not reproducible. Seeds below 1000 will use a high quality but slower PRNG (mersenne twister 32). Seeds above that will use the very fast but low quality PRNG ranlux24." << "\n";
      std::cout << "\t--jobs UINT      How many jobs to run in parallel. Default: 8." << "\n";
      std::cout << "\t--threads UINT   How many threads to use to run threads. If set to 0, uses one thread per job. Default: 0." << "\n";
      std::cout << "\t--slow UINT      In animations, how slow to render to be legiblie by human eyes. Default: 1." << "\n";
      std::cout << "\t--headless       Do not spawn a window. Stops window drawing loop. Also slightly better cpu utilisation." << "\n";
      exit(0);
    } else {
      throw std::invalid_argument("Unknown argument: '"s + argv[i] + "'.");
    }
  }

  std::unique_ptr<Canvas> canvas;
  if (!headless) {
    canvas = std::make_unique<Canvas>("Sentinel Evolution (seed " + std::to_string(config.seed) + ")");
    canvas->setColor({224,224,255}, Brush::background);
    canvas->setColor({64,32,32}, Brush::stroke);
  }

  switch (eng) {
    case Eng::sandbox: {
      run<Sandbox>(std::move(canvas), config);
    } break;
    case Eng::evolution: {
      run<Evolution>(std::move(canvas), config);
    } break;
    case Eng::parallel: {
      run<Parallel>(std::move(canvas), config);
    } break;
    case Eng::replay: {
      run<Replay>(std::move(canvas), config);
    } break;
    case Eng::demo: {
      run<Demo>(std::move(canvas), config);
    } break;
    default:{}
  }
}
