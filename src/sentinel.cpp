#include "sentinel.h"

#include <cmath>
#include <algorithm>
#include <limits>

namespace {
  static constexpr double tau = std::asin(1)*4;
}

Point Point::fromPolar(double const angle, double const length) {
  double const x = std::sin(tau*angle) * length;
  double const y = std::cos(tau*angle) * length;
  return {x,y};
}

constexpr Point Point::operator+(Point const pt) const {
  return {xy[0]+pt.xy[0], xy[1]+pt.xy[1]};
}
constexpr Point Point::operator-(Point const pt) const {
  return {xy[0]-pt.xy[0], xy[1]-pt.xy[1]};
}
constexpr double Point::operator*(Point const pt) const {
  return xy[0]*pt.xy[0] + xy[1]*pt.xy[1];
}
constexpr Point Point::operator*(double const s) const {
  return {xy[0]*s, xy[1]*s};
}
constexpr Point Point::operator/(double const s) const {
  return {xy[0]/s, xy[1]/s};
}
double norm(Point const pt) {
  return std::sqrt(pt*pt);
}
constexpr double Point::cross_product(Point const lhs, Point const rhs) {
  return lhs[0]*rhs[1] - lhs[1]*rhs[0];
}

Point Point::intersect(Point const a1, Point const a2, Point const a3, Point const a4) {
  double const div = (a1[0] - a2[0])*(a3[1] - a4[1]) - (a1[1] - a2[1])*(a3[0] - a4[0]);
  if (std::abs(div) < 1e-9) return {std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity()};
  double const sup = (a1[0] - a3[0])*(a3[1] - a4[1]) - (a1[1] - a3[1])*(a3[0] - a4[0]);
  double const t = sup / div;
  return a1 + (a2-a1)*t;
}

std::array<Point, 4> Bot::legPositions() const {
  std::array<Point, 4> points;
  for (std::size_t i = 0; i < 4; ++i) {
    points[i] = head + Point::fromPolar(angle + legs[i].angle/4 + static_cast<double>(i)/4, legs[i].extension * maxLeg);
  }
  return points;
}

void Bot::reset() {
  for (std::size_t i = 0; i < 4; ++i) {
    legs[i].angle = 0.5;
    legs[i].extension = 0.5;
    legs[i].status = Leg::Status::fixed;
  }
  head = Point{};
  angle = 0.;
}

void Bot::stomp() {
  for (std::size_t i = 0; i < 4; ++i) {
    legs[i].status = Leg::Status::fixed;
  }
}

void Bot::headForward() {
  for (std::size_t i = 0; i < 4; ++i) {
    if (legs[i].status == Leg::Status::lifted)
      return;
  }
  auto const pos = legPositions();
  double move = 161;
  Point const d = Point::fromPolar(angle, 1.);
  Point const p = head;
  for (std::size_t i = 1; i <= 2; ++i) {
    Point const q = pos[i];
    Point const u = p + d*(((q-p)*d) / (d*d));
    Point const up = p-u;
    Point const uq = q-u;
    double const max = std::sqrt(maxLeg*maxLeg - uq*uq) - norm(up);
    move = std::clamp(move, 0., max);
  }
  for (std::size_t j = 3; j <= 4; ++j) {
    auto const i = j%4;
    Point const q = pos[i];
    double const max = (((q-p)*d) / (d*d));
    move = std::min(max, move);
  }
  {
    Point const is = Point::intersect(p, p+d, pos[3], pos[0]);
    move = std::min(norm(is-p), move);
  }
  Point const v = p + d*(move/norm(d));
  head = v;

  // update feet
  for (std::size_t i = 0; i < 4; ++i) {
    auto const dir = (pos[i]-head)/norm(pos[i]-head);
    auto a = std::atan2(dir[0], dir[1]) / tau;
    if (a<0) a+=1;
    a = a*4 - static_cast<double>(i);
    legs[i].angle = a;
    legs[i].extension = norm(pos[i]-head) / maxLeg;
  }
}

bool Bot::stable() const {
  std::size_t num = 0;
  std::array<Point, 4> points;
  std::size_t i = 0;
  for (auto const pos: legPositions()) {
    if (legs[i++].status == Leg::Status::fixed) {
      points[num++] = pos;
    }
  }
  std::array<double, 4> sides;
  for (i = 0; i < num; ++i) {
    auto const line = points[(i+1)%num] - points[i];
    auto const probe = head - points[i];
    sides[i] = Point::cross_product(line, probe);
  }
  for (i = 0; i < num; ++i) {
    if (std::abs(sides[i]) < 1e-12) {
      sides[i] = sides[--num];
    }
  }
  for (i = 1; i < num; ++i) {
    if ((sides[i-1] < 0) != (sides[i] < 0))
      return false;
  }
  return true;
}
