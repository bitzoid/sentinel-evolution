#pragma once

#include <array>

struct Point {
  std::array<double, 2> xy;
  static Point fromPolar(double angle, double length);
  constexpr Point(double const x = 0., double const y = 0.) : xy{x,y} {}

  constexpr Point operator+(Point) const;
  constexpr Point operator-(Point) const;
  constexpr double operator*(Point) const;
  constexpr Point operator*(double) const;
  constexpr Point operator/(double) const;
  constexpr static double cross_product(Point, Point);
  friend double norm(Point);

  constexpr double operator[](std::size_t const idx) const {
    return xy[idx];
  }

  static Point intersect(Point, Point, Point, Point);
};

struct Range {
  double min;
  double max;
};

struct Leg {
  double angle; // 0..1
  double extension; // 0..1
  enum class Status {
    lifted, fixed
  };
  Status status = Status::fixed;
  constexpr static const Range angleRange{0.05, 0.95};
  constexpr static const Range extensionRange{0.4, 1.};
};

struct Bot {
  std::array<Leg, 4> legs;
  Point head{};
  double angle = 0;
  double maxLeg = 1;

  std::array<Point, 4> legPositions() const;
  void reset();
  void stomp();
  void headForward();
  bool stable() const;
};
